﻿namespace McOrders
{
    internal class Narudzba
    {
        public Narudzba(string naziv, int vrijeme, int prioritet)
        {
            Naziv = naziv;
            Vrijeme = vrijeme;

        }
        public string Naziv { get; set; }
        public int Vrijeme { get; set; }
        public override string ToString()
        {
            return Naziv;
        }
        public int Prioritet { get; set; }
    }
}
