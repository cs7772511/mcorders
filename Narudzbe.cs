﻿namespace McOrders
{
    internal class Narudzbe
    {
        private readonly List<Narudzba> narudzbaList;
        private readonly List<Narudzba> gotoveNarudzbeList;
        private readonly object _lock;

        public event EventHandler<Narudzba> NarudzbaDodana;
        public event EventHandler<Narudzba> NarudzbaGotova;
        public Narudzbe()
        {
            narudzbaList = new List<Narudzba>();
            gotoveNarudzbeList = new List<Narudzba>();
            _lock = new object();
            Prioritet = 0;
        }
        public void Naruci(Narudzba narudzba)
        {
            narudzbaList.Add(narudzba);
            Task.Run(() =>
            {
                narudzba.Prioritet++;
                Thread.Sleep(2000);
                lock (_lock)
                {
                    NarudzbaDodana?.Invoke(this, narudzba);
                    Prioritet++;
                }
                Thread.Sleep(narudzba.Vrijeme * 1000);
                lock (_lock)
                {
                    narudzbaList.Remove(narudzba);
                    gotoveNarudzbeList.Add(narudzba);
                }
                NarudzbaGotova?.Invoke(this, narudzba);
            });
        }
        public List<Narudzba> GetNarudzbe()
        {
            return narudzbaList.ToList();
        }
        public List<Narudzba> GetGotoveNarudzbe()
        {
            return gotoveNarudzbeList.ToList();
        }
        public int Prioritet { get; set; }
    }
}
