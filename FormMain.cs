namespace McOrders
{
    public partial class FormMain : Form
    {
        private readonly Narudzba pommes, bigMac, drink;
        private readonly Narudzbe narudzbe;
        public FormMain()
        {
            InitializeComponent();
            drink = new Narudzba("Drink", 2);
            bigMac = new Narudzba("Big Mac", 10);
            pommes = new Narudzba("Pommes", 5);
            narudzbe = new Narudzbe();

            narudzbe.NarudzbaDodana += Narudzba_OnOrderStarted;
            narudzbe.NarudzbaGotova += Narudzba_OnOrderCompleted;

        }
        private void PrioritetFormat()
        {

        }
        private void RefreshActiveOrders()
        {
            lbActiveOrders.Items.Clear();
            foreach (Narudzba narudzba in narudzbe.GetNarudzbe())
            {
                lbActiveOrders.Items.Add($"[{narudzbe.Prioritet}] {narudzba.Naziv}");
            }
        }
        private void RefreshCompletedOrders()
        {
            lbCompletedOrders.Items.Clear();
            foreach (Narudzba narudzba in narudzbe.GetGotoveNarudzbe())
            {
                lbCompletedOrders.Items.Add($"[{narudzbe.Prioritet}] {narudzba.Naziv}");
            }
        }

        private void Narudzba_OnOrderStarted(object sender, Narudzba narudzba)
        {

            Invoke(new Action(() =>
            {
                RefreshActiveOrders();
                RefreshCompletedOrders();
            }));
        }
        private void Narudzba_OnOrderCompleted(object sender, Narudzba narudzba)
        {
            int index = lbActiveOrders.Items.IndexOf($"[{narudzbe.Prioritet}] {narudzba.Naziv}"); ;
            Invoke(new Action(() =>
            {
                RefreshActiveOrders();
                RefreshCompletedOrders();
            }));
        }

        private void btnPommes_Click(object sender, EventArgs e)
        {
            Task.Run(() => narudzbe.Naruci(pommes));
        }

        private void btnBigMac_Click(object sender, EventArgs e)
        {
            Task.Run(() => narudzbe.Naruci(bigMac));
        }

        private void btnDrink_Click(object sender, EventArgs e)
        {
            Task.Run(() => narudzbe.Naruci(drink));
        }
    }
}